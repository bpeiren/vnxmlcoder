import XCTest
@testable import VNXMLCoder

final class VNXMLCoderTests: XCTestCase {
    func testExample() throws {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct
        // results.
        XCTAssertEqual(VNXMLCoder().text, "Hello, World!")
    }
}
